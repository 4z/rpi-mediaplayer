#!/bin/sh

function log() {
    echo "$@"
    echo "$@" >> "/tmp/audiodev"
}

log -n "waiting for pulseaudio ... "
while ! pidof "pulseaudio" > /dev/null ; do
    sleep 1
done
log "DONE"

pa_mods="$(pactl list short modules)"
if [ $? -ne 0 ] ; then
    log "Error listing pulse modules"
    exit
fi

if echo "$pa_mods" | grep module-alsa-sink > /dev/null ; then
    log "module-alsa-sink already loaded"
    exit
fi

log "Loading pulse ALSA module: "
n=0
while [ $n -lt 30 ] ; do
    n=$((n+1))
    log "Attempt $n / 30"
    ret="$(pactl load-module module-alsa-sink)"
    if [ $? -eq 0 ] ; then
        exit
    fi
    if [ "$ret" != "Failure: Module initialization failed" ] ; then
        echo "Unknown return string. Giving up"
        log "$ret"
        exit
    fi
    log "$ret"
    sleep 1
done

