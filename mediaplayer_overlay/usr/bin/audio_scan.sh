#!/bin/ash

function log() {
    echo "$@"
    echo "$@" >> "/tmp/audio_scan"
}

log "audio_scan.sh started"

# wait for dbus 
log -n "waiting for dbus ... "
while [ ! -e "/var/run/dbus-session.env" ] ; do            
        sleep 1
done
export DBUS_SESSION_BUS_ADDRESS="$(cat /var/run/dbus-session.env)"
log "DONE"

function get_player() {
    dbus-send --session --dest="org.freedesktop.DBus" --type=method_call --print-reply=literal "/org/freedesktop/DBus" "org.freedesktop.DBus.ListNames" |
        tr ' ' '\n' |
        grep "org.mpris.MediaPlayer2" |
        head -n 1
}

# wait until players interface appears
log -n "waiting for player ... "
player="$(get_player)"
while [ -z "$player" ] ; do
	sleep 1
	player="$(get_player)"
done
log "DONE"

function playback_status() {
    dbus-send --session --dest="$player" --print-reply "/org/mpris/MediaPlayer2" "org.freedesktop.DBus.Properties.Get" "string:org.mpris.MediaPlayer2.Player" "string:PlaybackStatus"  |
        grep "string" | sed 's/[^"]*"\([^"]*\).*/\1/'
}

function get_tracklist() {
    dbus-send --session --dest="$player" --print-reply "/org/mpris/MediaPlayer2" "org.freedesktop.DBus.Properties.Get" "string:org.mpris.MediaPlayer2.TrackList" "string:Tracks" |
        grep "object path " | sed 's/[^"]*"\([^"]*\).*/\1/' 
}

function add_to_playlist() {
    file="$1"
    play="false"
    
    dbus-send --session --dest="$player" --type=method_call --print-reply=literal "/org/mpris/MediaPlayer2" "org.mpris.MediaPlayer2.TrackList.AddTrack" \
        "string:file://$file" \
        "objpath:/org/mpris/MediaPlayer2/TrackList/Append" \
        "boolean:$play"

    log "added: $file"
}

function remove_track() {
    track_id="$1"
    log "removed: '$track_id'"
    dbus-send --session --dest="$player" --type=method_call --print-reply=literal "/org/mpris/MediaPlayer2" "org.mpris.MediaPlayer2.TrackList.RemoveTrack" \
        "objpath:$track_id"
}

function play() {
    dbus-send --session --dest="$player" --type=method_call "/org/mpris/MediaPlayer2" "org.mpris.MediaPlayer2.Player.Play"
}

log "Player at: '$player'"
log "Player status: '$(playback_status)'"

function enque_audio() {
    mkdir -p "/tmp/mediaplayer_tracklist/"
    tracks_file="/tmp/mediaplayer_tracklist/$(echo "$1" | sha1sum | cut -d ' ' -f 1)"
    rm -f "$tracks_file"

    tracklist="$(get_tracklist)"
    
    # too slow
    #find "$1" -type f -not -path '*/\.*' | xargs -n1 -I{} file --mime-type "{}" | 
    find "$1" -type f -not -path '*/\.*' | egrep '\.(mp3|flac|ogg|m4a)$' | sort |
    while read -r line ; do
	filename="$line"
        #filename=${line%%:*}
        #mime_type=${line##*:}
        #mime_category=${mime_type%%/*}
        
        #if [ "$mime_category" = " audio" ] ; then
            add_to_playlist "$filename"
        #fi
    done
    
    if [ "$(playback_status)" != "Playing" ] ; then
        play
    fi
    
    tracklist="$tracklist
$(get_tracklist)"

    added_tracks="$(echo "$tracklist" | sort | uniq -u | sed '/^$/d')"
    log "Added tracks:"
    log "$added_tracks"
    
    echo "$added_tracks" > "$tracks_file"

}

if [ "$2" = "removed" ] ; then
    log "removing tracks for '$1'"
    tracks_file="/tmp/mediaplayer_tracklist/$(echo "$1" | sha1sum | cut -d ' ' -f 1)"
    cat "$tracks_file" | 
    while read -r track_id ; do
        remove_track "$track_id"
    done
    rm -f "$tracks_file"
else
    log "scanning for audio in '$1'"
    enque_audio "$1"
fi
