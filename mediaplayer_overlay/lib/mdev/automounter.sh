#!/bin/ash 

destdir=/media

SCAN_PID_FILE="/var/run/audio_scan.pid"

function log() {
    echo "$@"
    echo "$@" >> "/tmp/automount"
}

function start_scan() {
    start-stop-daemon \
        --start \
        --background \
        --pidfile "$SCAN_PID_FILE" \
        --make-pidfile \
        --user mediaplayer \
        --chuid mediaplayer \
        --startas "/usr/bin/audio_scan.sh" -- \
        "$1"
}

function stop_scan() {
    [ ! -e "$SCAN_PID_FILE" ] && return
    start-stop-daemon \
        --stop \
        --retry TERM/5/KILL/10 \
        --user mediaplayer \
        --pidfile "$SCAN_PID_FILE"
    rm "$SCAN_PID_FILE"
}

function remove_tracks_from_playlist() {
    start-stop-daemon \
        --start \
        --background \
        --user mediaplayer \
        --chuid mediaplayer \
        --startas "/usr/bin/audio_scan.sh" -- \
        "$1" "removed"
}

do_umount() {
    if ! grep -qs "^/dev/$1 " /proc/mounts ; then
        return 
    fi
    [ ! -d "${destdir}/$1" ] && return 
    
    n=0
    while ! umount "${destdir}/$1" ; do
        log "retry $n"
        sleep 1
        n=$((n+1))
        [ $n -gt 30 ] && break
    done
	[ -d "${destdir}/$1" ] && rmdir "${destdir}/$1"
}

do_mount() {
	mkdir -p "${destdir}/$1" || exit 1

	if ! mount -o ro,noexec,iocharset=utf8 "/dev/$1" "${destdir}/$1"; then
		rmdir "${destdir}/$1"
		exit 1
	fi
}

case "${ACTION}" in
add|"")
	do_umount ${MDEV}
	do_mount ${MDEV}
	log "${MDEV} added"
	stop_scan
	start_scan "${destdir}/${MDEV}"
	;;
remove)
	do_umount ${MDEV}
	log "${MDEV} removed"
	stop_scan
	remove_tracks_from_playlist "${destdir}/${MDEV}"
	;;
esac
