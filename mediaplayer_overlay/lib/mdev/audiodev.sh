#!/bin/ash 

PID_FILE="/var/run/audio_add.pid"

function log() {
    echo "$@"
    echo "$@" >> "/tmp/audiodev"
}

function start_audio_add() {
    start-stop-daemon \
        --start \
        --background \
        --pidfile "$PID_FILE" \
        --make-pidfile \
        --startas "/usr/bin/audio_add.sh"
}

function stop_audio_add() {
    [ ! -e "$PID_FILE" ] && return
    start-stop-daemon \
        --stop \
        --retry TERM/5/KILL/10 \
        --pidfile "$PID_FILE"
    rm "$PID_FILE"
}

case "${ACTION}" in
add|"")
        log "${MDEV} added"
        # non blocking
        stop_audio_add
        start_audio_add
        ;;
remove)
        log "${MDEV} removed"
        stop_audio_add
        ;;
esac
