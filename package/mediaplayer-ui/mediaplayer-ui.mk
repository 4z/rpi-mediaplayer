################################################################################
#
# mediaplayer-ui
#
################################################################################

MEDIAPLAYER_UI_VERSION = 0d5cceea6379d557932e0ce359bd2605d6686e30
MEDIAPLAYER_UI_SITE = https://gitlab.com/4z/mediaplayer-ui.git
MEDIAPLAYER_UI_SITE_METHOD = git
MEDIAPLAYER_UI_SETUP_TYPE = setuptools
MEDIAPLAYER_UI_LICENSE = GPL

define MEDIAPLAYER_UI_USERS
	mediaplayer -1 mediaplayer -1 * - - -
endef

define MEDIAPLAYER_UI_INSTALL_INIT_SYSV
	$(INSTALL) -D -m 0755 package/mediaplayer-ui/S55mediaplayer-ui $(TARGET_DIR)/etc/init.d/S55mediaplayer-ui
endef

$(eval $(python-package))
