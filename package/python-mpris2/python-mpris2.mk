################################################################################
#
# python-mpris2
#
################################################################################

PYTHON_MPRIS2_VERSION = 1.0.2
PYTHON_MPRIS2_SOURCE = mpris2-$(PYTHON_MPRIS2_VERSION).tar.gz
PYTHON_MPRIS2_SITE = https://files.pythonhosted.org/packages/22/b8/ce1287ef272b0fed49653a1ccb9ffea68bc5acbbc63d8027e3e18b2ca9d6
PYTHON_MPRIS2_LICENSE = GPL
PYTHON_MPRIS2_SETUP_TYPE = distutils

$(eval $(python-package))
